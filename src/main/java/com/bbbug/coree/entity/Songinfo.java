package com.bbbug.coree.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Songinfo implements Serializable {
    private static final long serialVersionUID = -8442633469680405435L;
    private Integer id;
    private Long mid;
    private String name;
    private String pic;
    private String singer;
    private Integer length;
    private Integer hot;
    private String url;
}
