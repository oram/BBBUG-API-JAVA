package com.bbbug.coree.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class SongSubinfo implements Serializable {
    private static final long serialVersionUID = -7260712252411479343L;
    private Integer mid;
    private String name;
    private String pic;
    private String singer;
    private Integer length;
    private Integer played;
}
