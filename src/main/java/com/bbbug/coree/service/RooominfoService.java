package com.bbbug.coree.service;

import com.bbbug.coree.entity.Roominfo;
import com.bbbug.coree.entity.Roomuserinfo;
import com.bbbug.coree.mapper.RoominfoMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@EnableCaching
@Service

public class RooominfoService {
    @Resource
    RoominfoMapper mapper;

    @Cacheable(value = "roominfobyid", key = "#roomId", unless = "#result==null")
    public Roominfo queryById(Integer roomId) {
        return mapper.queryById(roomId);
    }

    public List<Roomuserinfo> queryAll() {
        return mapper.queryAll();
    }

    @CacheEvict(value = {"roominfosbrobbot", "roominfobyid"}, key = "#roominfo.room_id")
    public Roominfo insert(Roominfo roominfo) {
        boolean ans = mapper.insert(roominfo);
        return ans ? roominfo : null;
    }

    @CacheEvict(value = {"roominfosbrobbot", "roominfobyid"}, key = "#roominfo.room_id")
    public Roominfo update(Roominfo roominfo) {
        boolean ans = mapper.update(roominfo);
        return ans ? roominfo : null;
    }

    public List<Roominfo> queryAllsbrobot() {
        return mapper.queryAllsbrobot();
    }

    @Cacheable(value = {"roominfosbrobbot"}, unless = "#result == null")
    public Roominfo getroomneedsbronbotbyid(Integer roomId) {
        Roominfo tp = mapper.getroomneedsbronbotbyid(roomId);
        tp.setRoom_robot(tp.getRoom_robot() == null ? 0 : tp.getRoom_robot());
        return tp;
    }

    @Cacheable(cacheNames = {"roomadmin"}, key = "#adminid", unless = "#result.size()!=null&&#result.size()==0")
    public List<Roominfo> queryByadmin(Integer adminid) {
        return mapper.queryByadmin(adminid);
    }
}
