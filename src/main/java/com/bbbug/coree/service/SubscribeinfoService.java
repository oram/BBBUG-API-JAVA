package com.bbbug.coree.service;

import com.bbbug.coree.mapper.SubscribeinfoMapper;
import com.bbbug.coree.entity.Subscribeinfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service

public class SubscribeinfoService {
    @Resource
    SubscribeinfoMapper mapper;

    public List<Subscribeinfo> queryForBoth(Integer mid, Integer user_id) {
        return mapper.queryForBoth(mid, user_id);
    }

    public Boolean insert(Integer mid, Integer user_id, Integer played) {
        return mapper.insert(mid, user_id, played);
    }

    public Boolean update(Integer mid, Integer user_id) {
        return mapper.update(mid, user_id);
    }

    public Boolean delete(Integer mid, Integer user_id) {
        return mapper.delete(mid, user_id);
    }

    public List<Subscribeinfo> querybymid(Integer mid) {
        return mapper.querybymid(mid);
    }
}
