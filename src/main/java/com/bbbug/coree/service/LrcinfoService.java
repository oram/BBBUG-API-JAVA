package com.bbbug.coree.service;

import com.bbbug.coree.entity.Lrcinfo;
import com.bbbug.coree.mapper.LrcinfoMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service

public class LrcinfoService {
    @Resource
    LrcinfoMapper mapper;

    public Lrcinfo queryById(Integer id) {
        return mapper.queryById(id);
    }

    public Boolean insert(Integer id, String lrc) {
        return mapper.insert(id, lrc);
    }
}
