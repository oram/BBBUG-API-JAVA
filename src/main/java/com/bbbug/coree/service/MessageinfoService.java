package com.bbbug.coree.service;

import cn.hutool.json.JSONObject;
import com.bbbug.coree.entity.Messageinfo;
import com.bbbug.coree.mapper.MessageinfoMapper;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service

public class MessageinfoService {
    @Resource
    private MessageinfoMapper mapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    public Messageinfo queryById(String messageId){
        return mapper.queryById(messageId);
    }
    public List<Messageinfo> queryAllByLimit(int offset, int limit){
        return mapper.queryAllByLimit(offset,limit);
    }
    public List<Messageinfo> queryForList(String room_id){
        return mapper.queryForList(room_id);
    }
    public Boolean insert(Messageinfo messageinfo){
        return mapper.insert(messageinfo);
    }
    public Messageinfo update(Messageinfo messageinfo){
        return mapper.update(messageinfo);
    }
    public boolean deleteById(String messageId){
        return mapper.deleteById(messageId);
    }
    public boolean deleteByroomid(Integer room_id){return mapper.deleteByroomid(room_id);}
    public void sendMessage(String room_id, String content){
        JSONObject msg = new JSONObject();
        msg.set("room_id", room_id)
        .set("content", content);
        stringRedisTemplate.convertAndSend("topic", msg.toString());
    }
}
