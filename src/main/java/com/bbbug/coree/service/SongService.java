package com.bbbug.coree.service;

import com.bbbug.coree.mapper.SongMapper;
import com.bbbug.coree.entity.Songinfo;
import com.bbbug.coree.entity.SongSubinfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service

public class SongService {
    @Resource
    SongMapper mapper;

    public List<Songinfo> queryByLimit() {
        return mapper.queryByLimit();
    }

    public Songinfo queryByMid(Long mid) {
        return mapper.queryByMid(mid);
    }

    public Boolean insert(Songinfo songinfo) {
        return mapper.insert(songinfo);
    }

    public Boolean updateSong(Integer mid) {
        return mapper.updateSong(mid);
    }

    public List<SongSubinfo> queryByUserid(Integer user_id) {
        return mapper.queryByUserid(user_id);
    }

    public List<Songinfo> queryByUserUpload(String key) {
        return mapper.queryByUserUpload(key);
    }
}
