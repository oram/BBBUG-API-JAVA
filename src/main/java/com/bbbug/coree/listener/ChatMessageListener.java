package com.bbbug.coree.listener;


import cn.hutool.json.JSONObject;
import com.bbbug.coree.websocketservice.core;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;
import org.yeauty.pojo.Session;

import java.util.concurrent.ConcurrentHashMap;

@Component
public class ChatMessageListener implements MessageListener {
    @Override
    public void onMessage(Message message, byte[] bytes) {
        JSONObject msg = new JSONObject(message.toString());
        String room_id = msg.getStr("room_id");
        String content = msg.getStr("content");
        ConcurrentHashMap<String, Session> stringSessionConcurrentHashMap = core.CHATMAP.get(room_id);
        if (stringSessionConcurrentHashMap == null) {
            return;
        }
        for (Session value : stringSessionConcurrentHashMap.values()) {
            value.sendText(content);
        }
    }
}
