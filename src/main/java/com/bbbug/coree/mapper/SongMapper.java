package com.bbbug.coree.mapper;

import com.bbbug.coree.entity.Songinfo;
import com.bbbug.coree.entity.SongSubinfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SongMapper {
    @Select("select * from songinfo order by hot DESC LIMIT 0,30")
    List<Songinfo> queryByLimit();

    @Select("select * from songinfo where mid=#{mid}")
    Songinfo queryByMid(Long mid);

    @Insert("insert into songinfo (mid, name, pic, singer, length,hot, url) value (#{mid},#{name},#{pic},#{singer},#{length},#{hot}, #{url})")
    Boolean insert(Songinfo songinfo);

    @Update("update songinfo set hot=hot+1 where mid=#{mid}")
    Boolean updateSong(Integer mid);

    @Select("select songinfo.mid, name, pic, singer, length,played from songinfo,subscribeinfo where subscribeinfo.user_id=#{user_id} and subscribeinfo.mid=songinfo.mid")
    List<SongSubinfo> queryByUserid(Integer user_id);

    @Select("select * from songinfo where mid<0 and (name like concat(#{keyword},'%') or singer like concat(#{keyword},'%'))")
    List<Songinfo> queryByUserUpload(String keyword);


}
