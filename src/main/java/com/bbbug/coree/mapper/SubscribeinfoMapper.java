package com.bbbug.coree.mapper;

import com.bbbug.coree.entity.Subscribeinfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface SubscribeinfoMapper {
    @Select("select * from subscribeinfo where mid=#{mid} and user_id=#{user_id}")
    List<Subscribeinfo> queryForBoth(Integer mid, Integer user_id);

    @Insert("insert into subscribeinfo (mid,user_id,played) value (#{mid},#{user_id},#{played})")
    Boolean insert(Integer mid, Integer user_id, Integer played);

    @Update("update subscribeinfo set played=played+1 where mid=#{mid} and user_id=#{user_id}")
    Boolean update(Integer mid, Integer user_id);

    @Delete("DELETE FROM  subscribeinfo WHERE mid=#{mid} and user_id=#{user_id} ")
    Boolean delete(Integer mid, Integer user_id);

    @Select("select * from subscribeinfo where mid=#{mid} ")
    List<Subscribeinfo> querybymid(Integer mid);
}
