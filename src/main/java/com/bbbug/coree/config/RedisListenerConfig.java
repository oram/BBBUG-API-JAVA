package com.bbbug.coree.config;

import com.bbbug.coree.listener.ChatMessageListener;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

@Configuration
public class RedisListenerConfig extends CachingConfigurerSupport {

    /**
     * 消息监听容器
     *
     * @param factory
     * @return
     */
    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory factory) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(factory);
        //订阅一个通道 该处的通道名是发布消息时的名称
        container.addMessageListener(catAdapter(), new PatternTopic("topic"));
        return container;
    }

    /**
     * 消息监听适配器，绑定消息处理器
     *
     * @return
     */
    @Bean
    MessageListenerAdapter catAdapter() {
        return new MessageListenerAdapter(new ChatMessageListener());
    }

}
